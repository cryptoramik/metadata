def xml_compare():
    with open('../tasks.xml', "r", encoding="utf-8") as xml1, \
            open('../xml_result.xml', "r", encoding="utf-8") as xml2:
        return set(xml1).difference(set(xml2))


if __name__ == '__main__':
    with open('compare.xml', 'w') as result:
        for i in xml_compare():
            result.write(i)

import xml.dom.minidom as md
import argparse
import psycopg2
from convert.ram_pgs_ddl import create_pgs_ddl
from convert.converter import converter


def generate_db(schema_name):
    xml_ram = md.parse("tasks.xml")
    schema = converter(xml_ram)
    schema.name = schema_name
    postgresql_ddl = create_pgs_ddl(schema)
    connect = psycopg2.connect("dbname='{dbname}' user='{user}' host='{host}' password='{pwd}'".format(
        dbname="testdb",
        user="postgres",
        host="localhost",
        pwd=""
    ))
    cursor = connect.cursor()
    cursor.execute(postgresql_ddl)
    connect.close()
    del connect
    print('Yeah! DDL successful created')


if __name__ == '__main__':
    generate_db = argparse.ArgumentParser()
    generate_db.add_argument("schema_name", action="store", help="Schema name")
    args = generate_db.parse_args()
    generate_db(args.schema_name)

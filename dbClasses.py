class Domain:

    def __init__(self):
        self.name = None
        self.description = None
        self.type = None
        self.length = None
        self.char_length = None
        self.precision = None
        self.show_null = False
        self.summable = False
        self.case_sensitive = False
        self.show_lead_nulls = False
        self.thousands_separator = False
        self.scale = None
        self.width = None
        self.align = None


class Index:

    def __init__(self):
        self.name = None
        self.fulltext = False
        self.uniqueness = False
        self.clustered = False
        self.local = False
        self.field = None


class Field:

    def __init__(self):
        self.name = None
        self.table = None
        self.rname = None
        self.description = None
        self.domain = None
        self.position = None
        self.edit = False
        self.required = False
        self.input = False
        self.show_in_grid = False
        self.show_in_details = False
        self.is_mean = False
        self.autocalculated = False


class Constraint:

    def __init__(self):
        self.name = None
        self.kind = None
        self.items = None
        self.reference_type = None
        self.reference = None
        self.expression = None
        self.unique_key_id = None
        self.has_value_edit = False
        self.cascading_delete = False
        self.full_cascading_delete = False


class Table:

    def __init__(self):
        self.name = None
        self.description = None
        self.fields = []
        self.constraints = []
        self.indices = []
        self.add = False
        self.edit = False
        self.delete = False
        self.temporal_mode = False
        self.ht_table_flags = None
        self.access_level = None
        self.means = None
        self.uuid = None


class Schema:

    def __init__(self):
        self.name = None
        self.description = None
        self.fulltext_engine = None
        self.version = None
        self.domains = []
        self.tables = []

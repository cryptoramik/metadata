import argparse
import xml.dom.minidom as md
import sqlite3
import os
from convert.converter import converter
from convert.in_xml import in_xml
from convert.ram_to_dbd import ram_dbd
from convert.dbd_to_ram import dbd_ram


def xdb2dbd(xml, dbd):
    xml_ram = md.parse("tasks.xml")

    # XML -> RAM
    schema = converter(xml_ram)
    print('Yeah! xml -> ram finished successful')

    # RAM -> XML
    xml_result = in_xml(schema)
    with open(xml, "wb") as file:
        file.write(xml_result.toprettyxml(encoding="utf-8", indent="  "))
    print('Yeah! ram -> xml finished successful')

    # RAM -> DBD
    if os.path.exists(dbd):
        if os.path.isfile(dbd):
            os.remove(dbd)
    connect = sqlite3.connect(dbd)
    ram_dbd(schema, connect)
    connect.commit()
    connect.close()
    del connect

    # DBD -> RAM
    dbd_ram(dbd)
    print('Yeah! dbd -> ram finished successful')


if __name__ == '__main__':
    xml2dbd = argparse.ArgumentParser()
    xml2dbd.add_argument("xml", action="store", help="Path to .xml file")
    xml2dbd.add_argument("dbd", action="store", help="Path to .dbd file")
    args = xml2dbd.parse_args()
    xdb2dbd(args.xml, args.dbd)

# Collective development #

### Структура проекта ###

* converter.py - преобразование xml -> ram
* in_xml - преобразование ram -> xml
* ram_to_dbd - преобразование ram -> sqlite
* dbd_to_ram - преобразование sqlite -> ram
* ram_pgs_ddl - модуль для генерации PostgreSQL DDL из ram представления
* dbClasses - описание классов для представления объектов базы данных в ram
	* Schema
	* Table
	* Field
	* Domain
	* Constraint
	* Index
import xml.dom.minidom as md
import sqlite3
import os
import psycopg2
from convert.ram_pgs_ddl import create_pgs_ddl
from convert.converter import converter
from convert.in_xml import in_xml
from convert.ram_to_dbd import ram_dbd
from convert.dbd_to_ram import dbd_ram
from dbd_const import SQL_DBD_Init

xml_ram = md.parse("tasks.xml")

# XML -> RAM
schema = converter(xml_ram)
print('Yeah! xml -> ram finished successful')

# RAM -> XML
xml_result = in_xml(schema)
with open("xml_result.xml", "w") as file:
    file.write(xml_result.toprettyxml(encoding="utf-8").decode("utf-8"))
print('Yeah! ram -> xml finished successful')

# RAM -> DBD
if os.path.exists("result_dbd"):
    if os.path.isfile("result_dbd"):
        os.remove("result_dbd")
connect = sqlite3.connect("result_dbd")
ram_dbd(schema, connect)
connect.commit()
connect.close()

# DBD -> RAM
dbd_ram("result_dbd")
print('Yeah! dbd -> ram finished successful')

# Create PostgreSQL DDL
postgresql_ddl = create_pgs_ddl(schema)
connect = psycopg2.connect("dbname='{dbname}' user='{user}' host='{host}' password='{pwd}'".format(
    dbname="testdb",
    user="postgres",
    host="localhost",
    pwd=""
))
print(postgresql_ddl)
cursor = connect.cursor()
cursor.execute(postgresql_ddl)
connect.close()
del connect
print('Yeah! DDL successful created')

import sqlite3
from dbClasses import Domain, Index, Schema, Table, Constraint, Field


def dbd_ram(schema):
    conn = sqlite3.connect(schema)
    cursor = conn.cursor()
    schema = Schema
    schema.domains = select_domains(conn)
    schema.tables = select_tables(conn)
    conn.commit()
    conn.close()
    return schema


def select_domains(conn):
    cursor = conn.cursor()
    domains = []
    domain_attributes = cursor.execute("""
          SELECT 
          name, 
          description, 
          data_type_id, 
          align, 
          width, 
          length, 
          precision, 
          show_null, 
          summable, 
          case_sensitive, 
          show_lead_nulls, 
          thousands_separator, 
          char_length, 
          scale FROM dbd$domains""").fetchall()

    for attribute in domain_attributes:
        domain = Domain
        domain.name, domain.description, domain.type, domain.align, domain.width, domain.length, \
        domain.precision, domain.show_null, domain.summable, domain.case_sensitive, domain.show_lead_nulls, \
        domain.thousands_separator, domain.char_length, domain.scale = attribute
        domain.show_null, domain.show_lead_nulls, domain.thousands_separator, domain.summable, \
        domain.case_sensitive = map(bool, [domain.show_null, domain.show_lead_nulls, domain.thousands_separator,
                                           domain.summable, domain.case_sensitive])
        if domain.char_length:
            domain.char_length = str(domain.char_length)
        else:
            domain.char_length = None
        if domain.length:
            domain.length = str(domain.length)
        else:
            domain.length = None
        if domain.scale:
            domain.scale = str(domain.scale)
        else:
            domain.scale = None
        if domain.precision:
            domain.precision = str(domain.precision)
        else:
            domain.precision = None
        if domain.width:
            domain.width = str(domain.width)
        else:
            domain.width = None
        domain.type = cursor.execute("""\
              SELECT type_id 
              FROM dbd$data_types 
              WHERE dbd$data_types.id = ?""", (domain.type,)).fetchone()[0]
        domains.append(domain)
    return domains


def select_constraints(conn, tid):
    cursor = conn.cursor()
    constraints = []
    constraint_attributes = cursor.execute("""\
    SELECT id, 
    table_id, 
    name, 
    constraint_type, 
    reference, 
    unique_key_id, 
    has_value_edit, 
    cascading_delete, 
    expression
    FROM dbd$constraints
    WHERE dbd$constraints.table_id = ?""", (tid,)).fetchall()

    for attribute in constraint_attributes:
        constraint = Constraint
        _, table_id, constraint.name, constraint.kind, constraint.reference, constraint.unique_key_id, \
            constraint.has_value_edit, constraint.cascading_delete, constraint.expression = attribute
        constraint.has_value_edit, constraint.cascading_delete = map(bool, [constraint.has_value_edit,
                                                                            constraint.cascading_delete])
        constraint.items = cursor.execute("""\
                    SELECT name FROM dbd$fields\
                    WHERE dbd$fields.id = (\
                    SELECT field_id FROM dbd$constraint_details\
                    WHERE dbd$constraint_details.constraint_id = ?)""", (attribute[0],)).fetchone()[0]
        if constraint.kind == "PRIMARY":
            constraint.reference = None
        else:
            cursor.execute("""\
        SELECT name FROM dbd$tables WHERE dbd$tables.id = ?""", (constraint.reference,)).fetchone()[0]
        constraints.append(constraint)
    return constraints


def select_indices(conn, tid):
    cursor = conn.cursor()
    indices = []
    index_attributes = cursor.execute("""\
        SELECT 
        id, 
        name, 
        local, 
        kind
        FROM dbd$indices
        WHERE dbd$indices.table_id = ?""", (tid,)).fetchall()
    for attribute in index_attributes:
        index = Index
        if attribute[3] == "fulltext":
            index.fulltext = True
            index.uniqueness = False
        elif attribute[3] == "uniqueness":
            index.fulltext = False
            index.uniqueness = True
        else:
            index.fulltext = False
            index.uniqueness = False
        index.name, index.local = attribute[1:-1]
        index.field = cursor.execute("""\
            SELECT name FROM dbd$fields \
            WHERE dbd$fields.id = (\
            SELECT field_id FROM dbd$index_details\
            WHERE dbd$index_details.index_id = ?)""", (attribute[0],)).fetchone()[0]
        indices.append(index)
    return indices


def select_fields(conn, tid):
    fields = []
    cursor = conn.cursor()
    field_attributes = cursor.execute("""\
         SELECT name,
         russian_short_name,
         description,
         domain_id,
         can_input, 
         can_edit,
         show_in_grid, 
         show_in_details, 
         is_mean, 
         autocalculated, 
         required 
         FROM dbd$fields
         WHERE dbd$fields.table_id = ?""", (tid,)).fetchall()

    for attribute in field_attributes:
        field = Field
        field.name, field.rname, field.description, field.domain, field.input, field.edit, \
        field.show_in_grid, field.show_in_details, field.is_mean, field.autocalculated, field.required = attribute
        field.input, field.edit, field.show_in_grid, field.show_in_details, field.is_mean, field.autocalculated, \
        field.required = map(bool, [field.input, field.edit, field.show_in_grid, field.show_in_details,
                                    field.is_mean, field.autocalculated, field.required])
        field.domain = cursor.execute("""SELECT name FROM dbd$domains WHERE dbd$domains.id = ?""",
                                      (field.domain,)).fetchone()[0]
        fields.append(field)
    return fields


def select_tables(conn):
    tables = []
    cursor = conn.cursor()
    table_attributes = cursor.execute("""\
        SELECT id, 
        name, 
        description, 
        can_add, 
        can_edit, 
        can_delete, 
        temporal_mode, 
        means 
        FROM dbd$tables""").fetchall()
    for attribute in table_attributes:
        table = Table
        tid, table.name, table.description, table.add, table.edit, table.delete, \
        table.temporal_mode, table.means = attribute
        table.add, table.edit, table.delete = map(bool, [table.add, table.edit, table.delete])
        table.constraints = select_constraints(conn, tid)
        table.indices = select_indices(conn, tid)
        table.fields = select_fields(conn, tid)
        tables.append(table)
    return tables

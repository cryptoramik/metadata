import xml.dom.minidom as md
from dbClasses import Domain, Index, Schema, Table, Constraint, Field


# XML -> RAM model
def converter(xml_file):

    schema = Schema()

    for i, value in xml_file.documentElement.attributes.items():
        if i.lower() == "name":
            schema.name = value
        elif i.lower() == "description":
            schema.description = value
        elif i.lower() == "fulltext_engine":
            schema.description = value
        elif i.lower() == "version":
            schema.description = value
        else:
            raise ValueError("Incorrect attribute name \"{}\"".format(i))

    schema.domains = get_domains(xml_file)
    schema.tables = get_tables(xml_file)

    return schema


def get_tables(xml_file):
    tables = []
    for table in xml_file.getElementsByTagName("table"):
        current = Table()
        for i, value in table.attributes.items():
            if i.lower() == "name":
                current.name = value
            elif i.lower() == "description":
                current.description = value
            elif i.lower() == "ht_table_flags":
                current.ht_table_flags = value
            elif i.lower() == "access_level":
                current.access_level = value
            elif i.lower() == "means":
                current.means = value
            elif i.lower() == "props":
                prop_attr = value
                for j in prop_attr.split(", "):
                    if j == "add":
                        current.add = True
                    elif j == "edit":
                        current.edit = True
                    elif j == "delete":
                        current.delete = True
                    elif j == "temporal_mode":
                        current.temporal_mode = True
                    else:
                        raise ValueError("Invalid format of props string: {}".format(prop_attr))

        current.indices = get_indices(table)
        current.fields = get_fields(table)
        current.constraints = get_constraints(table)
        tables.append(current)

    return tables


def get_domains(xml_file):
    domains = []
    for domain in xml_file.getElementsByTagName("domain"):
        current = Domain()
        for i, value in domain.attributes.items():
            if i.lower() == "name":
                current.name = value
            elif i.lower() == "description":
                current.description = value
            elif i.lower() == "type":
                current.type = value
            elif i.lower() == "length":
                current.length = value
            elif i.lower() == "char_length":
                current.char_length = value
            elif i.lower() == "precision":
                current.precision = value
            elif i.lower() == "scale":
                current.scale = value
            elif i.lower() == "width":
                current.width = value
            elif i.lower() == "align":
                current.align = value
            elif i.lower() == "props":
                prop_attr = value
                for j in prop_attr.split(", "):
                    if j == "show_null":
                        current.show_null = True
                    elif j == "summable":
                        current.summable = True
                    elif j == "case_sensitive":
                        current.case_sensitive = True
                    elif j == "show_lead_nulls":
                        current.show_lead_nulls = True
                    elif j == "thousands_separator":
                        current.thousands_separator = True
                    else:
                        raise ValueError("Invalid format of props string: {}".format(prop_attr))

        domains.append(current)

    return domains


def get_indices(table):
    indices = []
    for index in table.getElementsByTagName("index"):
        current = Index()
        for i, value in index.attributes.items():
            if i.lower() == "name":
                current.name = value
            elif i.lower() == "field":
                current.field = value
            elif i.lower() == "props":
                prop_attr = value
                for j in prop_attr.split(", "):
                    if j == "fulltext":
                        current.fulltext = True
                    elif j == "uniqueness":
                        current.uniqueness = True
                    elif j == "clustered":
                        current.clustered = True
                    else:
                        raise ValueError("Invalid format of props string: {}".format(prop_attr))

        indices.append(current)

    return indices


def get_fields(table):
    fields = []
    count = 1
    for field in table.getElementsByTagName("field"):
        current = Field()
        current.position = str(count)
        for i, value in field.attributes.items():
            if i.lower() == "name":
                current.name = value
            elif i.lower() == "table":
                current.table = value
            elif i.lower() == "rname":
                current.rname = value
            elif i.lower() == "description":
                current.description = value
            elif i.lower() == "domain":
                current.domain = value
            elif i.lower() == "props":
                prop_attr = value
                for j in prop_attr.split(", "):
                    if j == "input":
                        current.input = True
                    elif j == "edit":
                        current.edit = True
                    elif j == "required":
                        current.required = True
                    elif j == "autocalculated":
                        current.autocalculated = True
                    elif j == "show_in_grid":
                        current.show_in_grid = True
                    elif j == "show_in_details":
                        current.show_in_details = True
                    elif j == "is_mean":
                        current.is_mean = True
                    else:
                        raise ValueError("Invalid format of props string: {}".format(prop_attr))

        fields.append(current)

    return fields


def get_constraints(table):
    constraints = []
    for constraint in table.getElementsByTagName("constraint"):
        current = Constraint()
        for i, value in constraint.attributes.items():
            if i.lower() == "name":
                current.name = value
            elif i.lower() == "kind":
                current.kind = value
            elif i.lower() == "items":
                current.items = value
            elif i.lower() == "reference_type":
                current.props = value
            elif i.lower() == "reference":
                current.reference = value
            elif i.lower() == "expression":
                current.expression = value
            elif i.lower() == "unique_key_id":
                current.unique_key_id = value
            elif i.lower() == "props":
                prop_attr = value
                for j in prop_attr.split(", "):
                    if j == "has_value_edit":
                        current.has_value_edit = True
                    elif j == "cascading_delete":
                        current.cascading_delete = True
                    elif j == "full_cascading_delete":
                        current.full_cascading_delete = True
                    else:
                        raise ValueError("Invalid format of props string: {}".format(prop_attr))

        constraints.append(current)

    return constraints

import sqlite3
from dbd_const import SQL_DBD_Init
from dbClasses import Domain, Index, Schema, Table, Constraint, Field
from itertools import count


def ram_dbd(schema, conn):
    cursor = conn.cursor()
    constraint_id = count(1)
    index_id = count(1)
    cursor.executescript(SQL_DBD_Init)
    cursor.execute("INSERT INTO dbd$schemas (name) VALUES ('{}')".format(schema.name))

    # Insert to dbd$domains
    cursor.executemany(
        """INSERT INTO dbd$domains (
              name,
              description,
              length,
              char_length,
              precision,
              scale,
              width,
              align,
              show_null,
              show_lead_nulls,
              thousands_separator,
              summable,
              case_sensitive,
              data_type_id)
              VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", [(
            domain.name,
            domain.description,
            domain.length,
            domain.char_length,
            domain.precision,
            domain.scale,
            domain.width,
            domain.align,
            domain.show_null,
            domain.show_lead_nulls,
            domain.thousands_separator,
            domain.summable,
            domain.case_sensitive,
            -1
        ) for domain in schema.domains]
    )

    cursor.execute(
        "CREATE TABLE dbd$tmp (domain_name VARCHAR NOT NULL, domain_type VARCHAR NOT NULL, domain_type_id VARCHAR)")
    cursor.executemany(
        "INSERT INTO dbd$tmp VALUES (?, ?, ?)", [(domain.name, domain.type, -1)
                                                 for domain in schema.domains])
    cursor.execute(
        """UPDATE dbd$domains SET data_type_id = (
           SELECT dbd$data_types.id FROM dbd$domains AS dm JOIN dbd$tmp ON dbd$tmp.domain_name = dbd$domains.name 
           JOIN dbd$data_types ON dbd$data_types.type_id = dbd$tmp.domain_type
           WHERE dm.name = name);
        """)
    cursor.execute("DROP TABLE dbd$tmp")

    # Insert to dbd$tables
    cursor.executemany(
        """INSERT INTO dbd$tables (
              schema_id,
              name,
              description,
              can_add,
              can_edit,
              can_delete,
              temporal_mode,
              means)
              VALUES (?, ?, ?, ?, ?, ?, ?, ?)""", [(
            "schema_id",
            table.name,
            table.description,
            table.add,
            table.edit,
            table.delete,
            table.temporal_mode,
            table.means
        ) for table in schema.tables]
    )

    cursor.execute("""
    UPDATE dbd$tables
    SET schema_id = (
    SELECT id FROM dbd$schemas
    WHERE dbd$schemas.name = ?)""", (schema.name,))

    # Insert to dbd$fields
    for table in schema.tables:
        if table.fields:
            for field in table.fields:
                cursor.execute("""
                INSERT INTO dbd$fields (
                table_id,
                position,
                name, 
                russian_short_name, 
                description, 
                domain_id, 
                can_input, 
                can_edit, 
                show_in_grid, 
                show_in_details, 
                is_mean, 
                autocalculated, 
                required)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)""", (
                    cursor.execute("""SELECT id FROM dbd$tables WHERE dbd$tables.name = ?""", (table.name,)).fetchone()[0],
                    field.position,
                    field.name,
                    field.rname,
                    field.description,
                    cursor.execute("""SELECT id FROM dbd$domains WHERE dbd$domains.name = ?""",
                                   (field.domain,)).fetchone()[0],
                    field.input,
                    field.edit,
                    field.show_in_grid,
                    field.show_in_details,
                    field.is_mean,
                    field.autocalculated,
                    field.required
                ))

    # Insert to dbd$constraints
    for table in schema.tables:
        if table.constraints:
            for constraint in table.constraints:
                table_id = cursor.execute("""SELECT id FROM dbd$tables WHERE dbd$tables.name = ? """,
                                          (table.name,)).fetchone()[0]
    cursor.execute(
        """INSERT INTO dbd$constraints (
              table_id, 
              name, 
              constraint_type, 
              reference, 
              unique_key_id, 
              has_value_edit, 
              cascading_delete, 
              expression)
              VALUES (?, ?, ?, ?, ?, ?, ?, ?)""", (
            table_id,
            constraint.name,
            constraint.kind,
            table_id,
            constraint.unique_key_id,
            constraint.has_value_edit,
            constraint.cascading_delete,
            constraint.expression)
    )

    # Insert to dbd$constraint_details
    position_number = 1
    for table in schema.tables:
        for constraint in table.constraints:
            cursor.execute("""
            INSERT INTO dbd$constraint_details (
            constraint_id, 
            position,
            field_id)
            VALUES (?, ?, ?)""", (
                next(constraint_id),
                position_number,
                cursor.execute("""SELECT id FROM dbd$fields WHERE dbd$fields.name = ?""", (constraint.items,)).fetchone()[0]
            ))
        position_number = position_number + 1

    # Insert to dbd$indices
    for table in schema.tables:
        if table.indices:
            for index in table.indices:
                cursor.execute("""
                INSERT INTO dbd$indices (
                table_id,
                name, 
                local, 
                kind)
                VALUES (?, ?, ?, ?)""", (
                    cursor.execute("""SELECT id FROM dbd$tables WHERE dbd$tables.name = ?""", (table.name,)).fetchone()[0],
                    index.name,
                    index.local,
                    None
                ))

    # Insert to dbd$index_details
    position_number = 1
    for table in schema.tables:
        for index in table.indices:
            cursor.execute("""
            INSERT INTO dbd$index_details (
            index_id, 
            position, 
            field_id, 
            expression, 
            descend)
            VALUES (?, ?, ?, ?, ?)""", (
                next(index_id),
                position_number,
                cursor.execute("""
                SELECT id FROM dbd$fields WHERE dbd$fields.name = ?""", (index.field,)).fetchone()[0],
                None,
                None
            ))
        position_number = position_number + 1
    print('Yeah! ram -> dbd finished successful')

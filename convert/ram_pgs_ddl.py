import itertools


def create_pgs_ddl(schema):
    domains = []
    indices = []
    constraints = []
    tables = []
    primary_keys = []

    for i in schema.domains:
        domains.append(create_domain(schema.name, i))
    for i in schema.tables:
        table, indices, primary_key, constraints = create_table(schema.name, i)
        tables.append(table)
        indices.extend(indices)
        primary_keys.extend(primary_key)
        constraints.extend(constraints)
    create_schema_base_tables = ";\n".join(itertools.chain(domains, tables, indices)) + ";\n"
    create_primary_keys = ";\n".join(primary_keys) + ";\n"
    create_constraints = ";\n".join(constraints) + ";\n"

    return """\
    BEGIN TRANSACTION;
    SET CONSTRAINTS ALL DEFERRED;
    """ + "CREATE SCHEMA \"{name}\";\n".format(name=schema.name) + \
           create_schema_base_tables + \
           create_primary_keys + \
           create_constraints + \
           "COMMIT;"


# Create DDL for domain
def create_domain(schema_name, domain):

    return """CREATE DOMAIN \"{schema_name}\".\"{domain_name}\" AS {type_name}""".format(
        schema_name=schema_name,
        domain_name=domain.name,
        type_name=get_domain_type(domain)
    )


# Create DDL for table
def create_table(s_name, table):
    fields = ",\n".join(map(lambda f: create_field(s_name, f), table.fields))
    create_table_str = """CREATE TABLE \"{schema_name}\".\"{table_name}\"(\n{fields})""".format(
        schema_name=s_name,
        table_name=table.name,
        fields=fields
    )
    indices = map(lambda i: create_index(s_name, table.name, i), table.indices)
    primary_keys = map(lambda j: create_constraint(s_name, table.name, j),
                       filter(lambda j: j.kind == "PRIMARY", table.constraints))
    constraints = map(lambda j: create_constraint(s_name, table.name, j),
                      filter(lambda j: j.kind != "PRIMARY", table.constraints))

    return create_table_str, list(indices), list(primary_keys), list(constraints)


# Create DDL for field
def create_field(schema_name, field):

    return "\"{name}\" \"{schema_name}\".\"{type_}\"".format(
        name=field.name,
        schema_name=schema_name,
        type_=field.domain if isinstance(field.domain, str) else get_domain_type(field.domain)
    )


# Create DDL for index
def create_index(schema_name, table_name, index):

    index_return = """CREATE {unique} INDEX {name} ON \"{schema_name}\".\"{table_name}\" (\"{field}\")""".format(
        unique="UNIQUE" if index.uniqueness else "",
        schema_name=schema_name,
        name="\"" + index.name + "\"" if index.name is not None else "",
        table_name=table_name,
        field=index.field
    )

    return index_return


# Create DDL for constraint
def create_constraint(schema_name, table_name, constraint):
    primary_key = """ALTER TABLE \"{schema_name}\".\"{table_name}\"
    ADD PRIMARY KEY (\"{items}\")""".format(
        schema_name=schema_name,
        table_name=table_name,
        items=constraint.items
    )

    foreign_key = """ALTER TABLE \"{schema_name}\".\"{table_name}\"
    ADD {name} FOREIGN KEY (\"{items}\")
    REFERENCES \"{schema_name}\".\"{reference}\"""".format(
        schema_name=schema_name,
        table_name=table_name,
        name="CONSTRAINT \"" + constraint.name + "\"" if constraint.name is not None else "",
        items=constraint.items,
        reference=constraint.reference
    )

    check_constraint = """ALTER TABLE \"{schema_name}\".\"{table_name}\"
    ADD {name} CHECK ({expression})""".format(
        schema_name=schema_name,
        table_name=table_name,
        name="CONSTRAINT \"" + constraint.name + "\"" if constraint.name is not None else "",
        items=constraint.items,
        expression=constraint.reference
    )

    unique_constraint = """ALTER TABLE \"{schema_name}\".\"{table_name}\"
    ADD {name} UNIQUE (\"{items}\")""".format(
        schema_name=schema_name,
        table_name=table_name,
        name="CONSTRAINT \"" + constraint.name + "\"" if constraint.name is not None else "",
        items=constraint.items
    )

    if constraint.kind == 'PRIMARY':
        return primary_key
    elif constraint.kind == 'FOREIGN':
        return foreign_key
    elif constraint.kind == 'CHECK':
        return check_constraint
    elif constraint.kind == 'UNIQUE':
        return unique_constraint


# Get domain types
def get_domain_type(domain):
    domain_types = dict(INTEGER="int", BLOB="bytea", BOOLEAN="boolean", BYTE="smallint", LARGEINT="bigint",
                        SMALLINT="smallint", WORD="smallint", DATE="date", TIME="time", MEMO="text")
    alter = None
    if domain.type in domain_types:
        return domain_types[domain.type]
    if domain.type in ['STRING', 'CODE']:
        if domain.length is not None:
            length = domain.length
        elif domain.char_length is not None:
            length = domain.char_length
        return "{t}({n})".format(
            t="varchar",
            n=length)
    if domain.type in 'FLOAT':
        if domain.precision is not None:
            params = domain.precision
            if domain.scale is not None:
                params += ", " + domain.scale
        alter = "{t}{p}".format(
            t="numeric",
            p="(" + params + ")" if params is not None else "")
    if alter is not None:
        return alter
    else:
        return "text"

import minidom_fixed as md
from dbClasses import Domain, Index, Schema, Table, Constraint, Field


# RAM -> XML
def in_xml(schema):

    xml = md.Document()
    xml_entry = xml.createElement("dbd_schema")
    if schema.fulltext_engine is not None:
        xml_entry.setAttribute("fulltext_engine", schema.fulltext_engine)
    if schema.version is not None:
        xml_entry.setAttribute("version", schema.version)
    if schema.name is not None:
        xml_entry.setAttribute("name", schema.name)
    if schema.description is not None:
        xml_entry.setAttribute("description", schema.description)

    xml_entry.appendChild(xml.createElement("custom"))

    domains = xml.createElement("domains")
    tables = xml.createElement("tables")

    for domain in schema.domains:
        domains.appendChild(export_domain(xml, domain))
    xml_entry.appendChild(domains)

    for table in schema.tables:
        tables.appendChild(export_table(xml, table))
    xml_entry.appendChild(tables)

    xml.appendChild(xml_entry)
    return xml


def export_domain(xml, domain):
    xml_entry = xml.createElement("domain")
    if domain.name is not None:
        xml_entry.setAttribute("name", domain.name)
    if domain.description is not None:
        xml_entry.setAttribute("description", domain.description)
    if domain.type is not None:
        xml_entry.setAttribute("type", domain.type)
    if domain.length is not None:
        xml_entry.setAttribute("length", domain.length)
    if domain.char_length is not None:
        xml_entry.setAttribute("char_length", domain.char_length)
    if domain.precision is not None:
        xml_entry.setAttribute("precision", domain.precision)
    if domain.scale is not None:
        xml_entry.setAttribute("scale", domain.scale)
    if domain.width is not None:
        xml_entry.setAttribute("width", domain.width)
    if domain.align is not None:
        xml_entry.setAttribute("align", domain.align)

    props = []
    if domain.summable:
        props.append("summable")
    if domain.show_null:
        props.append("show_null")
    if domain.case_sensitive:
        props.append("case_sensitive")
    if domain.show_lead_nulls:
        props.append("show_lead_nulls")
    if domain.thousands_separator:
        props.append("thousands_separator")

    if props != []:
        xml_entry.setAttribute("props", ", ".join(props))

    return xml_entry


def export_table(xml, table):
    xml_entry = xml.createElement("table")
    if table.name is not None:
        xml_entry.setAttribute("name", table.name)
    if table.description is not None:
        xml_entry.setAttribute("description", table.description)
    if table.ht_table_flags is not None:
        xml_entry.setAttribute("ht_table_flags", table.ht_table_flags)
    if table.access_level is not None:
        xml_entry.setAttribute("access_level", table.access_level)
    if table.means is not None:
        xml_entry.setAttribute("means", table.means)

    props = []
    if table.add:
        props.append("add")
    if table.edit:
        props.append("edit")
    if table.delete:
        props.append("delete")
    if table.temporal_mode:
        props.append("temporal_mode")

    if props != []:
        xml_entry.setAttribute("props", ", ".join(props))

    # Here we export constraints, indexes and fields
    for field in table.fields:
        xml_entry.appendChild(export_field(xml, field))
    for constraint in table.constraints:
        xml_entry.appendChild(export_constraints(xml, constraint))
    for index in table.indices:
        xml_entry.appendChild(export_indices(xml, index))

    return xml_entry


def export_field(xml, field):
    xml_entry = xml.createElement("field")
    if field.name is not None:
        xml_entry.setAttribute("name", field.name)
    if field.rname is not None:
        xml_entry.setAttribute("rname", field.rname)
    if field.description is not None:
        xml_entry.setAttribute("description", field.description)
    if field.domain is not None:
        xml_entry.setAttribute("domain", field.domain)
    if field.position is not None:
        xml_entry.setAttribute("position", field.position)

    props = []
    if field.edit:
        props.append("edit")
    if field.input:
        props.append("input")
    if field.show_in_grid:
        props.append("show_in_grid")
    if field.show_in_details:
        props.append("show_in_details")
    if field.is_mean:
        props.append("is_mean")
    if field.autocalculated:
        props.append("autocalculated")
    if field.required:
        props.append("required")

    if props != []:
        xml_entry.setAttribute("props", ", ".join(props))

    return xml_entry


def export_constraints(xml, constraint):
    xml_entry = xml.createElement("constraint")
    if constraint.name is not None:
        xml_entry.setAttribute("name", constraint.name)
    if constraint.kind is not None:
        xml_entry.setAttribute("kind", constraint.kind)
    if constraint.items is not None:
        xml_entry.setAttribute("items", constraint.items)
    if constraint.reference is not None:
        xml_entry.setAttribute("reference", constraint.reference)
    if constraint.reference_type is not None:
        xml_entry.setAttribute("reference_type", constraint.reference_type)
    if constraint.expression is not None:
        xml_entry.setAttribute("expression", constraint.expression)

    props = []
    if constraint.has_value_edit:
        props.append("has_value_edit")
    if constraint.cascading_delete:
        props.append("cascading_delete")
    if constraint.full_cascading_delete:
        props.append("full_cascading_delete")

    if props != []:
        xml_entry.setAttribute("props", ", ".join(props))

    return xml_entry


def export_indices(xml, index):
    xml_entry = xml.createElement("index")
    if index.name is not None:
        xml_entry.setAttribute("name", index.name)
    if index.field is not None:
        xml_entry.setAttribute("field", index.field)

    props = []
    if index.fulltext:
        props.append("fulltext")
    if index.uniqueness:
        props.append("uniqueness")
    if index.clustered:
        props.append("clustered")

    if props != []:
        xml_entry.setAttribute("props", ", ".join(props))

    return xml_entry
